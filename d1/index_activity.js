let fName = "John";
let lName = "Smith";
let age = 30;
let hobbies = ["Biking", "Mountain Climbing", "Swimming"];
let workAddress = {
	houseNumber: 32,
	street: "Washington",
	city: "Lincoln",
	state: "Nebraska"
}

console.log("First Name: " + fName);
console.log("Last Name: " + lName);
console.log("Age: " + age);
console.log("Hobbies:");
console.log(hobbies);
console.log("Work Address:");
console.log(workAddress);

let fullName = "Steve Rogers";
let curAge = 40
let friendList = ["Tony", "Bruce", "Thor", "Natasha", "Clint", "Nick"];
let profile = {
	username: "captain_america",
	fullName: "Steve Rogers",
	age: 40,
	isActive: false
}
let bestFriend = "Bucky Barnes";
let curLocation = "Arctic Ocean";

console.log("My full name is: " + fullName);
console.log("My current age is: " + curAge);
console.log("My friends are:");
console.log(friendList);
console.log("My Full Profile:");
console.log(profile);
console.log("My bestfriend is: " + bestFriend);
console.log("I was found in: " + curLocation);