console.log("Hello World");

// Syntax of console.log: console.log(variableOrStringToBeConsoled);

//It will comment parts of the code that gets ignored by the language

/*
	There are two types of comments
	1. The single line comment denoted by two slashes
	2. The multi-line comment denoted by slash and asterisk
*/

// [Section] Syntax, statements
	// Statements in programming are instructions that we tell the computer to perform
	// JS Statements usually end with semicolon (;)
	// Semicolons are not required in JS but we will use it to help us train to locate where statements end
	// A syntax in programming, it is the set of rules that we describe statements must be constructed
	// All lines/blocks of code should be written in specific manner to work. This is due to how these codes where initially programmed to function in a certain manner

// [Section] Variables
	// Variables are used to contain data
	// Any Information that is used by an application is stored in what we call the memory
	// When we create variables, certain portions of a device's memory is given a name that we call variable
	// This makes it easier for us to associate information stored in our devices to actual "names" information

// Declaring Variables
	// Declaring Variables - It tells our devices that a variable name is created and is ready to store data
	// Syntax
		/*let/const variableName;*/

	let myVariable = "Ada Lovelace";
	let variable;
	// This will cause an undefined variable because the declared variable does not have an initial value
	console.log(variable);
	// const keyword is used when the value of the variable won't change
	const constVariable = "John Doe";
	// console.log() is useful for printing values of variables or certain results of code into the browser's console
	console.log(myVariable);

	/*
		Guides in writing variables
			1. Use the let keyword followed by the variable name of your choosing and use the assignment operator (=) to assign value
			2. Variable names should start with lowercase character, use camelCasing for multiple words
			3. for constant variables, use the 'const' keyword
				Note: if we use const keyword in declaring a variable, we cannot change the value of its variable
			4. Variable names should be indicative (descriptive) of the value being stored to avoid confusion
	*/

	// Declare and initialize
		// Intializing variables - the instance when a variable is given its first/intial value
		//Syntax:
			// let/const variableName = intial value;

	let productName = "desktop computer";

	//Declartion
	let desktopName;
	console.log(desktopName);
	//Initialize of the value of variable desktopName
	desktopName = "Dell";
	console.log(desktopName);

	//Reassigning value
		//Syntax: variableName = newValue;
	productName = "Personal Computer";
	console.log(productName);

	const name = "Rafhael Danniel Arbis";
	console.log(name);
	// This reassignment will cause an error since we cannot change/reassign the initial value of constant variable
	/*name = "Not me";*/

	// This will cause an error because the productName variable was already taken
	/*let productName = "Laptop";*/

	//var vs let/const
		//Some of you may wonder why we used let and const keyword in declaring a variable when we search online, we usually see var
		//var - is also used in declaring variable but var is an ecmaScript1 feature [(1997)]

	console.log(batch);
	batch = "Batch 241";
	var batch;

	/////////////////
	//let/const local/global scope
	/*
		Scope essentially means where these variables are available for use

		let/const are block scope

		A block is a chunk of code bounded by {}
	*/

	let outerVariable = "Hello";

	{
		let innerVariable = "Hello Again";
			console.log(innerVariable);
			console.log(outerVariable);
	}


	// Multiple Variable Declaration

	let productCode = "DC017", productBrand = "Dell";
	console.log(productCode);
	console.log(productBrand);

	// Using a variable with a reserved keyword
	// const let = "Hello";
	// console.log(let);

// [Section] Data Types
	// Strings
	/*
		Strings are a series of characters that create a word, a phrase, a sentence, or anything related to creating a text
		Strings in JavaScript can be written using either a single quote ('') or a double quote ("")
	*/

	let country = "Philippines";
	let province = "Metro Manila";
	console.log(country);
	console.log(province);

	// Concatenate Strings
	// Multiple string values can be combined to create a single string using the "+" symbol

	let fullAddress = province + ', ' + country
	console.log(fullAddress);

	// Declaring a string using an escape character

	let greeting = 'I live in the country ' + country;
	console.log(greeting);

	let message = 'John\'s employees went home early';
	console.log(message);

	//"\n" - refers to creating a new line in between our text
	let mailAddress = "Metro Manila\n\nPhilippines";
	console.log(mailAddress);

	// Numbers
	// Integer/Whole Number
	let headCount = 32;
	console.log(headCount);
	console.log(typeof headCount);

	// Decimal Number
	let grade = 98.7;
	console.log(grade);
	console.log(typeof grade);

	// Exponential Notation
	let planetDistance = "28000";
	console.log(planetDistance);
	console.log(typeof planetDistance);

	// Combining text and strings
	console.log("John's grade is " + grade);

	// Boolean
	// Boolean values are normally used to store values relating to the state of certain things
	// True or Fales
	let isMarried = false;
	let isGoodConduct = true;
	console.log(isMarried);
	console.log(isGoodConduct);

	// Arrays
	// Arrays are a special kind data type that's used to store multiple values

	/*
		Syntax:
			let/const arrayName = [elementA, elementB ...];
	*/

	let grades = [98,92.1,90.1,94.7];
	console.log(grades);

	// Different Data Type
	// Storing different data types inside an array is not recommended
	let details = ["John", 32, true];
	console.log(details);

	// Objects
	// Objects are another special kind of data type that is used to mimic real world objects or items

	/*
		Synatx:
			let/const objectName = {
				propertyA: value,
				propertyB: value
			}
	*/

	let person = {
		firstName: "John",
		lastName: "Smith",
		age: 32,
		isMarried: false,
		contact:["09123456789", "8123-4567"],
		address: {
			houseNumber: "345",
			city: "Manila"
		}
	}
	console.log(person);

	/*
		Constant Objects and Arrays

	*/
	const anime = ["one piece", "one punch man", "your lie in february"]
	console.log(anime);
	// arrayName[indexNumber]
	anime[0] = ["kimetsu no yaiba"];
	console.log(anime);

	// Null
	// It is used to intentionally express the absence of a value in a variable declaration/initialization

	let spouse = null;

	let myNumber = 0;
	let myString = "";

	// Undefined
	// Represent the state of a variable that has been declared but without an assigned value

	let fullName;
	console.log(fullName);